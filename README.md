# ONIGAWARA Industry License

ONIGAWARA Industry Licenseとは、オニガワラ・インダストリの製品に付属されるライセンスです。
ライセンスは何種類か存在し、製品ごとに適したものが選択されます。

### [ONIGAWARA Industry Sardine License(O.I.S.License)](https://gitlab.com/mr.pumpkin247/license/blob/master/OISLicense.md)

　O.I.S.Lisenseはデータの様々な環境/アプリケーションでの使用を想定したスタンダードなライセンスです。
VRSNS(例：[VRChat](https://www.vrchat.net/))でのアバターとしての使用や、オンライン環境でのマーケットイベント/コンペティション等へ参加の為のデータ利用などを想定しています。

### [ONIGAWARA Industry Oil Sardine License(O.I.O.S.License)](https://gitlab.com/mr.pumpkin247/license/blob/master/OIOSLicense%20.md)

　O.I.O.S.LicenseはO.I.S.Licenseの利用範囲に加えて、ライセンスの定める範囲においてデータの再配布/再販売を許可するものになります。
利用者は、このライセンスが付属しているデータの一部または全てを`再配布/再販売の不可を明示する場合に限り`配布/販売することが出来ます。